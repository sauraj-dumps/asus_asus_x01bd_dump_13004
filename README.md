## sdm660_64-user 9 PKQ1 134 release-keys
- Manufacturer: asus
- Platform: sdm660
- Codename: ASUS_X01BD
- Brand: asus
- Flavor: sdm660_64-user
- Release Version: 9
- Id: PKQ1
- Incremental: 16.2017.2011.105-20210108
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Android/sdm660_64/sdm660_64:9/PKQ1/16.2017.2011.105-20210108:user/release-keys
- OTA version: WW_Phone-16.2017.2011.105-20210108
- Branch: sdm660_64-user-9-PKQ1-134-release-keys
- Repo: asus_asus_x01bd_dump_13004


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
